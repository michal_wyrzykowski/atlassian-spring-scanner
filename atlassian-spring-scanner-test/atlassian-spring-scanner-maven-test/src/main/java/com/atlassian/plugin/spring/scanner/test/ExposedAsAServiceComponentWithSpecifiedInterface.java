package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@ExportAsService(ExposedAsAServiceComponentInterface.class)
@Component
public class ExposedAsAServiceComponentWithSpecifiedInterface implements ExposedAsAServiceComponentInterface, DisposableBean
{
    @Override
    public void destroy() throws Exception
    {
    }

    @Override
    public void doStuff()
    {
    }
}
