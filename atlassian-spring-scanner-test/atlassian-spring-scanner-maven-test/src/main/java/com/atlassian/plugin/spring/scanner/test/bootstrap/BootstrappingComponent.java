package com.atlassian.plugin.spring.scanner.test.bootstrap;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.dynamic.contexts.DynamicContext;

import com.google.common.base.Joiner;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * This magic little class can dynamically cause other components to come into and out of existence.
 */
@SuppressWarnings ({ "SpringJavaAutowiringInspection", "FieldCanBeLocal", "UnusedDeclaration" })
@Profile ("bootstrap")
@Component
public class BootstrappingComponent
{
    private static final String[] CONFIG_PATHS = { "/defaultProfile/default-spring-scanner.xml" };
    private static final Joiner INTERFACE_NAME_JOINER = Joiner.on(", ");

    private final ApplicationContext parentContext;
    private final I18nHelper.BeanFactory beanFactory;
    private final BundleContext bundleContext;
    private final DynamicContext.Installer dynamicContextInstaller;
    private ConfigurableApplicationContext internalContext;

    @Autowired
    public BootstrappingComponent(@ComponentImport EventPublisher eventPublisher, @ComponentImport PluginAccessor pluginAccessor, ApplicationContext parentContext, BundleContext bundleContext, @ComponentImport I18nHelper.BeanFactory beanFactory)
    {
        this.parentContext = parentContext;
        this.beanFactory = beanFactory;
        this.bundleContext = bundleContext;

        Plugin plugin = pluginAccessor.getPlugin("com.atlassian.plugin.spring.scanner.test");

        dynamicContextInstaller = DynamicContext.installer(eventPublisher, bundleContext, plugin.getKey());
    }

    public Result bootstrapTheRestOfTheApplication()
    {
        long then = System.currentTimeMillis();
        if (internalContext == null)
        {
            this.internalContext = dynamicContextInstaller.useContext(CONFIG_PATHS, parentContext);

            return new Result(true, then);
        }
        else
        {
            return new Result(false, then);
        }
    }

    public Result shutdownNewContext()
    {
        long then = System.currentTimeMillis();
        if (internalContext != null)
        {
            dynamicContextInstaller.closeAndUseContext(internalContext, internalContext.getParent());
            internalContext = null;
            return new Result(true, then);
        }
        return new Result(false, then);
    }

    public Set<String> listBeans()
    {
        Set<String> beans = new TreeSet<String>();
        if (internalContext != null)
        {
            beans.addAll(buildBeanDescriptions(internalContext));
        }
        beans.addAll(buildBeanDescriptions(parentContext));
        return beans;
    }

    private Set<String> buildBeanDescriptions(ApplicationContext context)
    {
        final Set<String> beans = new HashSet<String>();
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String name : beanDefinitionNames)
        {
            beans.add(name + " -> " + context.getBean(name));
        }
        return beans;
    }

    public Set<String> listServices()
    {
        final Set<String> services = new TreeSet<String>();
        for (final ServiceReference serviceReference : bundleContext.getBundle().getRegisteredServices())
        {
            final Long serviceId = (Long) serviceReference.getProperty(Constants.SERVICE_ID);
            final String[] interfaces = (String[]) serviceReference.getProperty(Constants.OBJECTCLASS);
            services.add("Id " + serviceId + " -> " + INTERFACE_NAME_JOINER.join(interfaces));
        }
        return services;
    }

    static class Result
    {
        boolean tookPlace;
        long timeTaken;

        Result(final boolean tookPlace, final long then)
        {
            this.tookPlace = tookPlace;
            this.timeTaken = System.currentTimeMillis() - then;
        }

        public boolean tookPlace()
        {
            return tookPlace;
        }

        public long getTimeTaken()
        {
            return timeTaken;
        }
    }
}
