package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings ({ "FieldCanBeLocal", "UnusedDeclaration" })
@Component
public class ConsumingMixedComponents
{
    private final IssueService issueService;
    private final InternalComponent internalComponent;
    private final InternalComponent2 internalComponent2;
    private final I18nHelper.BeanFactory beanFactory;


    @Autowired
    public ConsumingMixedComponents(@ComponentImport final IssueService issueService, final InternalComponent internalComponent, final InternalComponent2 internalComponent2, @ComponentImport I18nHelper.BeanFactory beanFactory)
    {
        this.issueService = issueService;
        this.internalComponent = internalComponent;
        this.internalComponent2 = internalComponent2;
        this.beanFactory = beanFactory;
    }
}
