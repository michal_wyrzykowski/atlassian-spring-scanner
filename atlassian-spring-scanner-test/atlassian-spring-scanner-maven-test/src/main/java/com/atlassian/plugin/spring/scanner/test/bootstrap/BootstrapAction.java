package com.atlassian.plugin.spring.scanner.test.bootstrap;

import java.io.PrintWriter;
import java.util.Set;

import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 */
@SuppressWarnings ("UnusedDeclaration")
public class BootstrapAction extends JiraWebActionSupport
{
    private final BootstrappingComponent bootstrappingComponent;
    private PrintWriter out;

    public BootstrapAction(final BootstrappingComponent bootstrappingComponent)
    {
        this.bootstrappingComponent = bootstrappingComponent;
    }

    @Override
    protected String doExecute() throws Exception
    {
        out = getHttpResponse().getWriter();
        out.print("<html><body>");

        displayBody(out, bootstrappingComponent.listBeans(), bootstrappingComponent.listServices());

        out.print("</body></html>");
        return NONE;
    }

    public String doStartup() throws Exception
    {
        out = getHttpResponse().getWriter();
        out.print("<html><body>");
        BootstrappingComponent.Result result = bootstrappingComponent.bootstrapTheRestOfTheApplication();
        if (result.tookPlace())
        {
            out.print(String.format("Bootstrapped....in %d ms", result.getTimeTaken()));
        }
        else
        {
            out.print("Oop...looks like we are already bootstrapped!");
        }

        displayBody(out, bootstrappingComponent.listBeans(), bootstrappingComponent.listServices());

        out.print("</body></html>");
        return NONE;
    }

    public String doShutdown() throws Exception
    {
        out = getHttpResponse().getWriter();
        out.print("<html><body>");

        BootstrappingComponent.Result result = bootstrappingComponent.shutdownNewContext();
        if (result.tookPlace())
        {
            out.print(String.format("The internal components have been shutdown in %d ms", result.getTimeTaken()));
        }
        else
        {
            out.print("The shutdown as previously taken place");
        }

        displayBody(out, bootstrappingComponent.listBeans(), bootstrappingComponent.listServices());
        out.print("</body></html>");
        return NONE;
    }

    private void links()
    {
        out.print("</br></br><div>"
                + "<a href='BootStrapAction!startup.jspa'>Startup</a> "
                + "<a href='BootStrapAction!shutdown.jspa'>Shutdown</a> "
                + "<a href='BootStrapAction.jspa'>List</a> "
                + "<a href='DefaultAction.jspa'>Default Action</a> "
                + "</div>");
    }

    private void displayBody(final PrintWriter out, final Set<String> beans, final Set<String> services)
    {
        displaySection(out, "The following components are defined in the app context:", beans);
        displaySection(out, "The following services are exported by this bundle:", services);
        links();
    }

    private void displaySection(final PrintWriter out, final String title, final Set<String> things)
    {
        out.print("<h3>"  + title + "</h3>" + "<div><ul>");
        for (final String thing : things)
        {
            out.print(String.format("<li><pre>%s</pre></li>", thing));
        }
        out.print("</ul></div>");
    }
}
