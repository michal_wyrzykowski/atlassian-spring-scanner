package com.atlassian.plugin.spring.scanner.test.external;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class ConsumesExternalComponentsViaFields
{
     @ClasspathComponent
     ExternalJarComponentComposite externalJarComponentComposite;
}
