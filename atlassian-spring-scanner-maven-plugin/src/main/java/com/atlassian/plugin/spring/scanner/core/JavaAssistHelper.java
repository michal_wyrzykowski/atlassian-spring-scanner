package com.atlassian.plugin.spring.scanner.core;

/**
 * @deprecated since 1.2.6, to be removed in 2.0.0: use JavassistHelper instead.
 */
@Deprecated
public class JavaAssistHelper extends JavassistHelper
{
}
