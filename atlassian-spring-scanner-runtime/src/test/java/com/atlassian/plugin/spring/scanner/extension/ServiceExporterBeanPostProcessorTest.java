package com.atlassian.plugin.spring.scanner.extension;

import com.atlassian.plugin.spring.scanner.extension.testservices.PrivateService;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicDevService;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicDevServiceProxy;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicNonInterfaceService;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicNonInterfaceServiceProxy;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicService;
import com.atlassian.plugin.spring.scanner.extension.testservices.PublicServiceProxy;
import com.atlassian.plugin.spring.scanner.extension.testservices.Service;
import org.apache.commons.collections.keyvalue.DefaultMapEntry;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

import static com.atlassian.plugin.spring.scanner.extension.IsAopProxy.aopProxy;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceExporterBeanPostProcessorTest
{
    @Mock private Bundle bundle;
    @Mock private BundleContext bundleContext;
    @Mock private ConfigurableListableBeanFactory beanFactory;
    @Mock private ExportedSeviceManager exportedSeviceManager;

    private ServiceExporterBeanPostProcessor exporterPostProcessor;
    private ApplicationContext applicationContext;

    @Before
    public void setUp() throws Exception
    {
        when(bundle.getBundleContext()).thenReturn(bundleContext);
        when(bundleContext.getBundle()).thenReturn(bundle);

        System.setProperty(ServiceExporterBeanPostProcessor.ATLASSIAN_DEV_MODE_PROP, Boolean.TRUE.toString());
        this.exporterPostProcessor = new ServiceExporterBeanPostProcessor(bundleContext, beanFactory, exportedSeviceManager);
        this.exporterPostProcessor.afterPropertiesSet();

        this.applicationContext = new ClassPathXmlApplicationContext("classpath:/com/atlassian/plugin/spring/scanner/extension/testservices/testservicesContext.xml");
    }

    @Test
    public void testNonProxyExportAsService() throws Exception
    {
        testNonProxyBeanIsExported(PublicService.class, Service.class);
    }

    @Test
    public void testNonProxyExportAsDevService() throws Exception
    {
        testNonProxyBeanIsExported(PublicDevService.class, Service.class);
    }

    @Test
    public void testNonProxyNonInterfaceExportAsService() throws Exception
    {
        testNonProxyBeanIsExported(PublicNonInterfaceService.class, PublicNonInterfaceService.class);
    }

    @Test
    public void testProxyExportAsService() throws Exception
    {
        testProxyBeanIsExported(PublicServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyExportAsDevService() throws Exception
    {
        testProxyBeanIsExported(PublicDevServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyNonInterfaceExportAsService() throws Exception
    {
        testProxyBeanIsExported(PublicNonInterfaceServiceProxy.class, PublicNonInterfaceServiceProxy.class);
    }

    @Test
    public void testPrivateServiceNotExported() throws Exception
    {
        testBeanIsExported(PrivateService.class, CoreMatchers.any(Object.class), CoreMatchers.any(Class.class), false);
    }

    private void testBeanIsExported(Class<?> beanImplClass, Matcher<Object> beanMatcher, Matcher<Class> interfaceMatcher, boolean expectExported) throws Exception
    {
        Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(beanImplClass);
        Assert.assertThat(nameBeanEntry.getValue(), beanMatcher);
        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedSeviceManager, expectExported ? times(1) : never()).registerService(
                any(BundleContext.class),
                eq(nameBeanEntry.getValue()),
                eq(nameBeanEntry.getKey()),
                anyMap(),
                argThat(interfaceMatcher));
    }

    private void testNonProxyBeanIsExported(Class<?> beanImplClass, Class expectInstanceOf) throws Exception
    {
        testBeanIsExported(beanImplClass, not(aopProxy()), equalTo(expectInstanceOf), true);
    }

    private void testProxyBeanIsExported(Class<?> beanImplClass, Class expectInstanceOf) throws Exception
    {
        testBeanIsExported(beanImplClass, aopProxy(), equalTo(expectInstanceOf), true);
    }

    protected  Map.Entry<String, Object> getBeanByImplClass(Class<?> beanImplClass)
    {
        final String beanName = getBeanDefaultName(beanImplClass);
        final Object bean = applicationContext.getBean(beanName);
        Assert.assertThat(bean, CoreMatchers.notNullValue());
        return new DefaultMapEntry(beanName, bean);
    }

    private String getBeanDefaultName(Class<?> type)
    {
        String name = type.getSimpleName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

 }