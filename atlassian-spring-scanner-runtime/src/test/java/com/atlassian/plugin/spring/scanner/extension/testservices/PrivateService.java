package com.atlassian.plugin.spring.scanner.extension.testservices;

import org.springframework.stereotype.Component;

@Component
public class PrivateService implements Service {
    @Override
    public void a() {
    }
}
