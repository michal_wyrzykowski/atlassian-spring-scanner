package com.atlassian.plugin.spring.scanner.util;

import com.atlassian.plugin.spring.scanner.extension.OsgiServiceFactoryBeanFactory;
import com.atlassian.plugin.spring.scanner.extension.gemini.GeminiOsgiServiceFactoryBeanFactory;
import com.atlassian.plugin.spring.scanner.extension.springdm.SpringOsgiServiceFactoryBeanFactory;

/**
 * Utility class to check what type of DM framework to use.  Atlassian Plugins < 3.1 uses SpringDM. Atlassian Plugins
 * >=3.1 uses Gemini Blueprint.  This singleton will return the appropriate implemenation hidden behind generic
 * interfaces
 * <p/>
 * Due to the Spring Framework adopting the CADT development model (http://www.jwz.org/doc/cadt.html) the latest version
 * of Spring no longer provides SpringDM.  This became Gemini Blueprint.  Same thing, different name, same bugs (plus
 * new ones!).  This class is smart enough to figure out if SpringDM or GeminiBlueprints is in use and will return the
 * appropriate implementation hidden behind a generic interface.  Atlassian Plugins 3.1 introduces Gemini Blueprints.
 */
public class SpringDMUtil
{
    private static SpringDMUtil INSTANCE;

    public static final String SPRING_DM_CLASS = "org.springframework.osgi.service.importer.support.OsgiServiceProxyFactoryBean";
    public static final String GEMINI_DM_CLASS = "org.eclipse.gemini.blueprint.service.importer.support.OsgiServiceProxyFactoryBean";

    private OsgiServiceFactoryBeanFactory osgiServiceFactoryBeanFactory;

    private SpringDMUtil()
    {
        try
        {
            //check if spring dm is available. If not it means we're running atlassian-plugins 3.1 which provides gemini blueprint instead.
            Class.forName(SPRING_DM_CLASS);
            osgiServiceFactoryBeanFactory = new SpringOsgiServiceFactoryBeanFactory();
        }
        catch (ClassNotFoundException e)
        {
            try
            {
                Class.forName(GEMINI_DM_CLASS);
                osgiServiceFactoryBeanFactory = new GeminiOsgiServiceFactoryBeanFactory();
            }
            catch (ClassNotFoundException cnfe)
            {
                throw new RuntimeException("Couldn't load spring dm nor gemini blueprint osgi service proxy factory bean class!", cnfe);
            }
        }
    }

    public static SpringDMUtil getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new SpringDMUtil();
        }
        return INSTANCE;
    }

    public OsgiServiceFactoryBeanFactory getOsgiServiceFactoryBeanFactory()
    {
        return osgiServiceFactoryBeanFactory;
    }
}
