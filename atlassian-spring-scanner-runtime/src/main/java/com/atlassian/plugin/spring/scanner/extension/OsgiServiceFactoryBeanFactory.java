package com.atlassian.plugin.spring.scanner.extension;

import org.osgi.framework.BundleContext;

import java.util.Map;

/**
 * Used for creating OsgiServiceFactoryBeans regardless of if the implementation is based on SpringDM or Gemini
 * Blueprints
 */
public interface OsgiServiceFactoryBeanFactory
{
    GenericOsgiServiceFactoryBean createExporter(BundleContext bundleContext, Object bean, String beanName, Map<String, Object> serviceProps, Class<?>[] interfaces)
            throws Exception;

    Class getProxyClass();
}
